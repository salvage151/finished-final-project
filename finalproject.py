#Liam Wilson
#10/5/15
#Choose your own adventure

#open multiple files
cho1 = open('allChoice/' + '1.0' + '.txt', 'r' )
cho2 = open('allChoice/' + '2.0' + '.txt' , 'r')
cho21 = open('allChoice/' + '2.1' + '.txt' , 'r')
cho22 = open('allChoice/' + '2.2' + '.txt' , 'r')
cho23 = open('allChoice/' + '2.3' + '.txt' , 'r')
cho211 = open('allChoice/' + '2.1.1' + '.txt' , 'r')
cho212 = open('allChoice/' + '2.1.2' + '.txt' , 'r')
cho213 = open('allChoice/' + '2.1.3' + '.txt' , 'r')
cho2111 = open('allChoice/' + '2.1.1.1' + '.txt' , 'r')
cho2112 = open('allChoice/' + '2.1.1.2' + '.txt' , 'r')
cho2113 = open('allChoice/' + '2.1.1.3' + '.txt' , 'r')
cho221 = open('allChoice/' + '2.2.1' + '.txt' , 'r')
cho222 = open('allChoice/' + '2.2.2' + '.txt' , 'r')
cho223 = open('allChoice/' + '2.2.3' + '.txt' , 'r')
cho231 = open('allChoice/' + '2.3.1' + '.txt' , 'r')
cho232 = open('allChoice/' + '2.3.2' + '.txt' , 'r')
cho233 = open('allChoice/' + '2.3.3' + '.txt' , 'r')
cho2211 = open('allChoice/' + '2.2.1.1' + '.txt' , 'r')
cho2121 = open('allChoice/' + '2.1.2.1' + '.txt' , 'r')
choDome = open('allChoice/' + 'Leave.Dome' + '.txt' , 'r')
choRuins = open('allChoice/' + 'Ruins' + '.txt' , 'r')
choCave = open('allChoice/' + 'Cave' + '.txt' , 'r')
choCaveEnd = open('allChoice/' + 'Cave.Ending' + '.txt' , 'r')
choReactorEnd = open('allChoice/' + 'Reactor.Ending' + '.txt' , 'r')
choLock = open('allChoice/' + 'Lockpick' + '.txt' , 'r')
choGuard = open('allChoice/' + 'Call.Guard' + '.txt' , 'r')
choDream = open('allChoice/' + 'Dream1' + '.txt' , 'r')
choCont = open('allChoice/' + 'Continue' + '.txt' , 'r')
choWake = open('allChoice/' + 'Wake.Up' + '.txt' , 'r')

print(cho1.read() + '\n')
print(cho2.read())

cho = int(input("Selection: " + '\n'))
def path1():
	print(cho21.read())
	cho = int(input("Selection: " + '\n'))
	if cho == 1:
		print(cho211.read())
		cho = int(input("Selection: " + '\n'))
		if cho == 1:
			print(cho2111.read())
			cho = int(input("Selection: " + '\n'))
			if cho == 1:
				Dream()
			elif cho == 2:
				print("The End")
			elif cho == 3:
				leaveDome()
		elif cho == 2:
			print(cho2112.read())
			cho = int(input("Selection: " + '\n'))
			if cho == 1:     						#Hospital
				Dream()								#To Sleep
			elif cho == 2:
				print("The End")					#Spoon
			elif cho == 3:
				leaveDome()
		elif cho == 3:
			print(cho2113.read())
			cho = int(input("Selection: " + '\n'))
			if cho == 1:
				CallGuard()
			elif cho == 2:
				Lockpick()
			elif cho == 3:
				Dream()
	elif cho == 2:
		print(cho212.read())
		cho = int(input("Selection: " + '\n'))
		if cho == 1:
			print(cho2121.read())
			cho = int(input("Selection: " + '\n'))
			if cho == 1:
				CallGuard()
			elif cho == 2:
				Lockpick()
			elif cho == 3:
				Dream()
		elif cho == 2:
			path3()
		elif cho == 3:
			leaveDome()
	elif cho == 3:
		print(cho213.read())
		cho = int(input("Selection: " + '\n'))
		if cho == 1:
			CallGuard()
		elif cho == 2:
			Lockpick()
		elif cho == 3:
			Dream()
def path2():
	print(cho22.read())
	cho = int(input("Selection: " + '\n'))
	if cho == 1:
		print(cho221.read())
		cho = int(input("Selection: " + '\n'))
		if cho == 1:
			print(cho2211.read()) #Jail
			cho = int(input("Selection: " + '\n'))
			if cho == 1:
				CallGuard()
			elif cho == 2:
				Lockpick()
			elif cho == 3:
				Dream()
		elif cho == 2:
			path1()
	elif cho == 2:
		print(cho222.read())
		cho = int(input("Selection: " + '\n'))
		if cho == 1:     						#Hospital
			Dream()
		elif cho == 2:
			print("The End")					#Spoon
		elif cho == 3:
			leaveDome()
	elif cho == 3:
		print(cho223.read())
		cho = int(input("Selection: " + '\n'))
		if cho == 1:
			CallGuard()
		elif cho == 2:
			Lockpick()
		elif cho == 3:
			Dream()
def path3():
	print(cho23.read())
	cho = int(input("Selection: " + '\n'))
	if cho == 1:
		print(cho231.read())
		cho = int(input("Selection: " + '\n'))
		if cho == 1:     						#Hospital
			path1()
		elif cho == 2:
			path2()					#Spoon
		elif cho == 3:
			leaveDome()
	elif cho == 2:
		print(cho232.read())
		cho = int(input("Selection: " + '\n'))
		if cho == 1:     						#Hospital
			Dream()
		elif cho == 2:
			print("The End")					#Spoon
		elif cho == 3:
			leaveDome()
	elif cho == 3:
		path1()
	elif cho == 4:
		path2()

def Ruins():
	print(choRuins.read()) #Goto Ruins from Cave
	cho = int(input("Selection: " + '\n'))
	if cho == 1:
		print(choReactorEnd.read())#Reactor End from Cave
	elif cho == 2:
		print("No no no, you no get to go back. You get struck by a meteor. You are dead. I laugh, hahaha.")

def Cave():
	print(choCave.read())			#Goto Cave
	cho = int(input("Selection: " + '\n'))
	if cho == 1:
		print(choCaveEnd.read())	#Cave End
	elif cho == 2:
		print("No no no, you no get to go back. You get struck by a meteor. You are dead. I laugh, hahaha.")

def leaveDome():
	print(choDome.read())				#Leave Dome
	cho = int(input("Selection: " + '\n'))
	if cho == 1:
		Ruins()
	elif cho == 2:
		Cave()

def Dream():							#Dream
	print(choDream.read())
	cho = int(input("Selection: " + '\n'))
	if cho == 1:
		print(choWake.read())
	elif cho == 2:
		print(choCont.read())

def Lockpick():							#Lockpick
	print(choLock.read())
	print(cho2111.read())
	cho = int(input("Selection: " + '\n'))
	if cho == 1:
		Dream()
	elif cho == 2:
		print("The End")
	elif cho == 3:
		leaveDome()

def CallGuard():						#Guard
	print(choGuard.read())
	leaveDome()

if cho == 1:
	path1()
elif cho == 2:
	path2()
elif cho == 3:
	path3()