You decide to go for a swim. The technician sees you and yells at you to get out because you're contaminating the water.
You promptly refuse. He calls the Sheriff and he arrests you. You are sitting in a cell with the other small time criminals of the town. You see that the sign behind the
deputy's desk says "Marlona, First town on Mars!". You wonder if it could really be true. You ask another prisoner, a
thief, if you're actually on Mars "Why yes-um, we are indeed on Mars, we colonized very quickly after some botanist was
stranded here, I think he's somewhere in the bio-dome."
------
Do you:
1 Call for a guard
2 Try to pick the lock
3 Take off your shirt, ball it up into a pillow, and go to sleep