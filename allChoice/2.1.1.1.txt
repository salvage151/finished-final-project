You wake up in the hospital. The doctor says "We managed to remove the bullet from your
abdomen, you should be OK but you should rest up."
------
Do you:
1: Go back to sleep.
2: Grab a spoon and try to stab yourself
3: Rip off all the monitors and walk out the door.